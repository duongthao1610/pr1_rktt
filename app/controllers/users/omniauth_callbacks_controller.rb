# frozen_string_literal: true

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  # You should configure your model like this:
  # devise :omniauthable, omniauth_providers: [:twitter]

  # You should also create an action method in this controller like this:
  # def twitter
  # end

  # More info at:
  # https://github.com/plataformatec/devise#omniauth

  # GET|POST /resource/auth/twitter
  def passthru
    binding.pry
    super
  end

  # GET|POST /users/auth/twitter/callback
  def failure
      redirect_to root_path
  end

  protected

  # The path used when OmniAuth fails
  def after_omniauth_failure_path_for(scope)
    super(scope)
  end

   def facebook
      binding.pry
      generic_callback("facebook")
    end

    def google_oauth2
      generic_callback( "google_oauth2" )
    end

    def generic_callback(provider)
      if user_signed_in?
        @social = Social.from_omniauth(request.env["omniauth.auth"])
        current_user.social.build(social)
        if @social.save!
          flash[:success] = "Lien ket thanh cong"
          redirect_to edit_user_path
      else
        flash[:fail] = "Khong the lien ket"
        redirect_to root_url
        end
      else
        flash[:fail] = "signin/ signup before"
        redirect_to root_url
      end
      binding.pry
      @social = @identity || current_user
      if @user.persisted?
        sign_in_and_redirect @user, :event => :authentication
        set_flash_message(:notice, :success, kind: provider.capitalize) if is_navigational_format?
      else
        session["devise.#{provider}_data"] = request.env["omniauth.auth"]
        redirect_to new_user_registration_url
      end
    end
end
