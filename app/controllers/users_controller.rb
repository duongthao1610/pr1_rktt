
class UsersController < ApplicationController


  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to admins_users_path
  end

  def show
    @user = User.find(params[:id])
  end

  private
  def user_params
    params.require(:user).permit User::ATTRIBUTES_PARAMS
  end
end
