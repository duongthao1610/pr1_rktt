Rails.application.routes.draw do
  root 'pages#home'

  devise_for :admins, controllers: {
      sessions: 'admins/sessions',
  }

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    omniauth_callbacks: "users/omniauth_callbacks"
  }
  get 'auth/failure', :to => 'session#failure'
  resources :users

  namespace :admins do
    root "users#index"
    resources :users
    end
end

