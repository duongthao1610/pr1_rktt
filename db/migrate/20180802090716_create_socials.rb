class CreateSocials < ActiveRecord::Migration[5.2]
  def change
    create_table :socials do |t|
      t.string :provider
      t.string :uid
      t.string :string
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :social, [:user_id, :created_at]
  end
end
